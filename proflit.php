<?php include "./php/head.php";

$numberOjBooks = 9; // количество книг отображаемых на главной странице
$numberOjBooksMobile = 4; // количество книг отображаемых на главной странице в мобильной версии
?>

<header>
    <div class="container">

        <div class="headerin">
            <div class="logohead">
                <img class="modlogo" src="img/logotip.png" alt="">
            </div>
            <nav class="navhead">
                <a class="navlink " href="index.php">Главная</a>
                <a class="navlink scrollToDown" href="product.php">Товары</a>
                <a class="navlink scrollToDown" href="surprise.php">Сюрприз</a>
                <a class="navlink scrollToDown" href="#contact">Контакты</a>
                <a class="navlink scrollToDown" target="_blank" href="politic.php">Конфеденциальность</a>
            </nav>
        </div>
    </div>
</header>

<div class="backgroundscreen">
    <div class="bigball"></div>
    <div class="ball"></div>
    <div class="smallball"></div>
</div>

<section class="first-screen">
    <div class="maintext">
        <h1>Профессиональная литература</h1>
        <p>Займись делом, а лучше сделай это правильно.</p>

    </div>
    <div class="book">
        <img src="./img/book.png" alt="">
    </div>
</section>

<section class="info">
    <div class="prime">
        <div class="hoverinprime">
            <img class="primeimg" src="img/cash.png" alt="">
            <div class="primeitem">
                <div class="primetitle">Бонусы</div>
                <div class="primetext">Сделай покупку и получи промокод</div>
            </div>
        </div>
        <div class="hoverinprime">
            <img class="primeimg" src="img/shop.png" alt="">

            <div class="primeitem">
                <div class="primetitle">В любое время</div>
                <div class="primetext">Покупай книги в любое время</div>
            </div>
        </div>
        <div class="hoverinprime">
            <img class="primeimg" src="img/drive.png" alt="">

            <div class="primeitem">
                <div class="primetitle">Доставка</div>
                <div class="primetext">Получи книгу по новой почте</div>
            </div>
        </div>
    </div>
</section>




<section class="product">

    <?php
    require_once 'php/Mobile_Detect.php';
    $detect = new Mobile_Detect;

    $objectWStatus = [];

    $q = 0;
    for ($j = 0; $j<count($objects); $j++) {
        if ($objects[$j]['type'] == false) {
            $objectWStatus[$q] = $objects[$j];
            $q++;
        }

    }



    foreach ($objectWStatus

    as $object):

    ?>
    <form action="second.php" method="get">
        <?php

        print('<input type="hidden" name="image" value="./img/products/' . $object["image"] . '">');
        print('<input type="hidden" name="name" value="' . $object["name"] . '">');
        print('<input type="hidden" name="text" value="' . $object["text"] . '">');
        print('<input type="hidden" name="info" value="' . $object["info"] . '">');
        print('<input type="hidden" name="price" value="' . $object["price"] . '">');
        print('<button class="block">');

        ?>
        <div class="photo">
            <?php print('<img src="./img/products/' . $object["image"] . '" alt="" class="image">'); ?>
            <p><?php print($object["info"]); ?></p>
        </div>
        <img src="./img/products/" alt="">
        <div class="textblock">
            <h2><?php print($object["name"]); ?></h2>

        </div>
        <div class="price">
            <h4><?php print($object["price"]); ?></h4>
            <p>UAH</p>
        </div>


        <?php echo "</button></form>";
        endforeach ?>
</section>

<?php include "./php/footer.php" ?>
