<?php include "./php/head.php";

$numberOjBooks = 9; // количество книг отображаемых на главной странице
$numberOjBooksMobile = 4; // количество книг отображаемых на главной странице в мобильной версии
?>

<header>
    <div class="container">

        <div class="headerin">
            <div class="logohead">
                <img class="modlogo" src="img/logotip.png" alt="">
            </div>
            <nav class="navhead">
                <a class="navlink " href="index.php">Главная</a>
                <a class="navlink activelink scrollToDown" href="product.php">Товары</a>
                <a class="navlink scrollToDown" href="./present.php">Сюрприз</a>
                <a class="navlink scrollToDown" href="#contact">Контакты</a>
                <a class="navlink scrollToDown" target="_blank" href="politic.php">Конфеденциальность</a>
            </nav>
        </div>
    </div>
</header>

<div class="backgroundscreen">
    <div class="bigball"></div>
    <div class="ball"></div>
    <div class="smallball"></div>
</div>

<section class="first-screen">
    <div class="maintext">
        <h1>Все книги</h1>
        <p>Выбери себе одну из множества наших книг</p>

    </div>
    <div class="book">
        <img src="./img/book.png" alt="">
    </div>
</section>


<section class="classifick">
    <a href="./classik.php" class="servicesfotoX">
        <div class="servicesinimgX">
            <div class="gradientsX">
                <img class="servimgX" src="./img/classik.jpg" alt="">
            </div>
            <div class="animtextX">Вечная классика</div>
            <div class="animtextbotX">Сборка литературы не для чтения, а для души.</div>
        </div>
    </a>
    <a href="./proflit.php" class="servicesfotoX">
        <div class="servicesinimgX">
            <div class="gradientsX">
                <img class="servimgX" src="./img/proflit.jpg" alt="">
            </div>
            <div class="animtextX">Профессиональная литература</div>
            <div class="animtextbotX">Займись делом, а лучше сделай это правильно.</div>
        </div>
    </a>
    <a href="./present.php" class="servicesfotoX">
        <div class="servicesinimgX">
            <div class="gradientsX">
                <img class="servimgX" src="./img/surprise.jpg" alt="">
            </div>
            <div class="animtextX">Сюрприз</div>
            <div class="animtextbotX">Случайная книга со скидкой -60% после покупок более чем на 1000грн.</div>
        </div>
    </a>
</section>

<div class="texth1">
    <p>Здесь показанны все книги которые мы можем вам предложить</p>
    <h1>Книги:</h1>
</div>

<section class="product">

    <?php



    foreach ($objects

    as $object):

    ?>
    <form action="second.php" method="get">
        <?php

        print('<input type="hidden" name="image" value="./img/products/' . $object["image"] . '">');
        print('<input type="hidden" name="name" value="' . $object["name"] . '">');
        print('<input type="hidden" name="text" value="' . $object["text"] . '">');
        print('<input type="hidden" name="info" value="' . $object["info"] . '">');
        print('<input type="hidden" name="price" value="' . $object["price"] . '">');
        print('<button class="block">');

        ?>
        <div class="photo">
            <?php print('<img src="./img/products/' . $object["image"] . '" alt="" class="image">'); ?>
            <p><?php print($object["info"]); ?></p>
        </div>
        <img src="./img/products/" alt="">
        <div class="textblock">
            <h2><?php print($object["name"]); ?></h2>

        </div>
        <div class="price">
            <h4><?php print($object["price"]); ?></h4>
            <p>UAH</p>
        </div>


        <?php echo "</button></form>";
        endforeach ?>
</section>

<?php include "./php/footer.php" ?>
