<!-- footer start -->
<footer>
    <div class="logot ft1">
    <span id="flx">
      <span class="npb npb-ft">
      </span>
    </span>
        <h2> <span class="sqaer">&#8226;</span> BOOK-SHOP</h2>
    </div>

    <hr class="animhr">

    <div class="coums-ft">
        <div class="col-1" id="contact">
            <p><span class="sqaer">&#8226;</span> Режим работы: Пн. – Пт.: с 9:00 до 18:00 <br>
                Телефон: 0666666666 <br>
                E-mail: knigarazym@gmail.com</p>
            <br>
            <br>
            <p><span class="sqaer">&#8226;</span> Фізична особа підприємець <br>
                Petya Ivanov <br>
                IПН 132453647 <br>
                IBAN 1234567890765432456789234, платник єдиного податку, <br>
                тел. +38 (066) 666 66 66</p>
        </div>
    </div>
    <div class="glb">
        <p>Читай книги - познавай мир</p>
    </div>
    <div class="footr-ft">
        <p class="prava">Да-да-да... всё под защитой</p>
        <p class="cop">2021 book-store</p>
    </div>
</footer>
<!-- footer end -->
</body>
</html>
