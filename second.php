<?php include "./php/head.php";

// получаем данные
if ($_GET) {
    $image = $_GET['image'];
    $name = $_GET['name'];
    $text = $_GET['text'];
    $info = $_GET['info'];
    $price = $_GET['price'];
} else {
    $image = $_POST['image'];
    $name = $_POST['name'];
    $text = $_POST['text'];
    $info = $_POST['info'];
    $price = $_POST['price'];
}

ini_set('display_errors',0);
$city = $_POST['newmail'];
$num = $_POST['num'];
$number = $_POST['number'];
$FIO = $_POST['FIO'];
$post = $_POST['post'];

?>
<?php

// получаем токен
$url = "https://innsmouth.payhub.com.ua/auth/token";

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

$headers = array(
    "accept: application/json",
    "Content-Type: application/json",
);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

// вот тут надо внести логин пароль
$data = '{"params":{"login":"pay_test","password":"password","client":"transacter"}}';

curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

//for debug only!
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

$resp = curl_exec($curl);
curl_close($curl);
$response = json_decode($resp, true);
//var_dump($response['data']['access_token']); //вот наш токен

?>
<?php


//uuid нужен чтобы придать каждой операции определённый не повторяющийся ключ, так же все операции должны храниться в базе
$uuid = array(
    'time_low' => 0,
    'time_mid' => 0,
    'time_hi' => 0,
    'clock_seq_hi' => 0,
    'clock_seq_low' => 0,
    'node' => array()
);

$uuid['time_low'] = mt_rand(0, 0xffff) + (mt_rand(0, 0xffff) << 16);
$uuid['time_mid'] = mt_rand(0, 0xffff);
$uuid['time_hi'] = (4 << 12) | (mt_rand(0, 0x1000));
$uuid['clock_seq_hi'] = (1 << 7) | (mt_rand(0, 128));
$uuid['clock_seq_low'] = mt_rand(0, 255);

for ($i = 0; $i < 6; $i++) {
    $uuid['node'][$i] = mt_rand(0, 255);
}

$uuid = sprintf('%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x',
    $uuid['time_low'],
    $uuid['time_mid'],
    $uuid['time_hi'],
    $uuid['clock_seq_hi'],
    $uuid['clock_seq_low'],
    $uuid['node'][0],
    $uuid['node'][1],
    $uuid['node'][2],
    $uuid['node'][3],
    $uuid['node'][4],
    $uuid['node'][5]
);



// получаем ссылку на платёжку
$url = "https://innsmouth.payhub.com.ua/frames/links/pga";

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);


$headers = array(
    "accept: application/json",
    "Authorization: Bearer " . $response['data']['access_token'],
    "Content-Type: application/json",
);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

// тут вводим кучу своих данных
$data = '{"external_id":"' . $uuid . '", "options": {"backurl":{"success":"https://book-shop.com.ua/succes.php","error":"https://book-shop.com.ua/error.php","cancel":"https://book-shop.com.ua/cancel.php"}},"lang":"UK","title":"Покупка на book-shop.com.ua","amount":' . $price . '00,"description":"' . $info . '","short_description":"Покупка книги","merchant_config_id":"113e6da5-29a3-4575-b0e8-306c3fca566b","config_id":"f23d69ca-9032-4282-afe0-9b1ae95b5026","hold":true}';

curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

//for debug only!
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

$resp = curl_exec($curl);
curl_close($curl);
$link = json_decode($resp, true);
$linkes = $link['url']; //вот ссылка. вставляеться она на 204 строке

?>
<header>
    <div class="container">

        <div class="headerin">
            <div class="logohead">
                <img class="modlogo" src="img/logotip.png" alt="">
            </div>
            <nav class="navhead">
                <a class="navlink " href="index.php">Главная</a>
                <a class="navlink scrollToDown" href="product.php">Товары</a>
                <a class="navlink scrollToDown" href="present.php">Сюрприз</a>
                <a class="navlink scrollToDown" href="#contact">Контакты</a>
                <a class="navlink scrollToDown" target="_blank" href="politic.php">Конфеденциальность</a>
            </nav>
        </div>
    </div>
</header>

<div class="backgroundscreen">
    <div class="bigball"></div>
    <div class="ball"></div>
    <div class="smallball"></div>
</div>

<section class="first-screen">
    <div class="maintext">
        <p><?php echo $name; ?></p>
        <div><?php print($info); ?></div>
    </div>
    <div class="book">
        <?php print('<img src="' . $image . '" alt="">'); ?>
    </div>
</section>

<form method="post" action="second.php" class="buy">
      <?php
    print('<input style="dislay:none" type="hidden" name="image" value="' . $image . '">');
    print('<input style="dislay:none" type="hidden" name="name" value="' . $name . '">');
    print('<input style="dislay:none" type="hidden" name="text" value="' .$text . '">');
    print('<input style="dislay:none" type="hidden" name="info" value="' . $info . '">');
    print('<input style="dislay:none" type="hidden" name="price" value="' . $price . '">');
    ?>
    <div class="inputs">
        <div class="adrees">
            <h2 class="namer">Город:</h2>
            <input class="inputer" name="newmail" type="text" required>
            <input style="display: none" name="post" type="number" value="1">
        </div>
        <br>
        <div class="number">
            <h2 class="num">Отеделние Новой Почты:</h2>
            <input class="inputer" name="num" type="number" required>
        </div>
        <br>
        <div class="number">
            <h2 class="num">мобильный номер:</h2>
            <input class="inputer" name="number" type="number" required>
        </div>
        <br>
        <div class="number">
            <h2 class="num">ФИО получателя:</h2>
            <input class="inputer" name="FIO" type="text" required>
        </div>
    </div>
    <div class="buyinfo"><?php print($text); ?>
        <p>Убедитесь что вы внесли все данные. Совершая покупку вы соглашаетесь с <a href="politic.php">Условиями Конфеденциальности</a></p>
    </div>

    <div class="buybutton">
        <script>
            function newlink($site) {

                window.open($site);
                // alert('Вы получили промокод для покупки книги сюрприз! \n key: ilovebooks');
            }
        </script>
        <p>Цена: <?php print($price); ?>грн</p>
        <?php print('<button onclick="newlink(\''.$linkes.'\')" class="button">Купить</button>'); ?>
    </div>
</form>

<?php include "./php/footer.php" ?>



