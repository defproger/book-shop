<?php include "./php/head.php";

$numberOjBooks = 9; // количество книг отображаемых на главной странице
$numberOjBooksMobile = 4; // количество книг отображаемых на главной странице в мобильной версии
?>

    <header>
        <div class="container">

            <div class="headerin">
                <div class="logohead">
                    <img class="modlogo" src="img/logotip.png" alt="">
                </div>
                <nav class="navhead">
                    <a class="navlink " href="index.php">Главная</a>
                    <a class="navlink scrollToDown" href="product.php">Товары</a>
                    <a class="navlink scrollToDown" href="./present.php">Сюрприз</a>
                    <a class="navlink scrollToDown" href="#contact">Контакты</a>
                    <a class="navlink scrollToDown activelink" target="_blank" href="politic.php">Конфеденциальность</a>
                </nav>
            </div>
        </div>
    </header>

    <div class="backgroundscreen">
        <div class="bigball"></div>
        <div class="ball"></div>
        <div class="smallball"></div>
    </div>

    <section class="first-screenPOLITIC">
        <div class="maintextPOLITIC">
            <div>
                Политика в отношении обработки персональных данных <br>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
            </div>
    </section>


<?php include "./php/footer.php" ?>
<?php
