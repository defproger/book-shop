$(window).scroll(function() {
    if ($(this).scrollTop() > 1){
        $('header').addClass("sticky");
    }
    else{
        $('header').removeClass("sticky");
    }

    if ($(this).scrollTop() > 100){
        $('.ball').addClass("ballanim");
    }
    else{
        $('.ball').removeClass("ballanim");
    }

    if ($(this).scrollTop() > 50){
        $('.smallball').addClass("smallballballanim");
    }
    else{
        $('.smallball').removeClass("smallballballanim");
    }

    if ($(this).scrollTop() > 50){
        $('.bigball').addClass("bigballanim");
    }
    else{
        $('.bigball').removeClass("bigballanim");
    }
});
